"use strict";

const tabs = document.getElementById("tabs");
const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content > li");

function clickHandler(e) {
  let champion = e.target.textContent;
  tabsTitle.forEach((e) => {
    e.classList.remove("active");
  });
  tabsContent.forEach((e) => {
    if (e.dataset.champion === champion) {
      e.classList.add("active");
    } else {
      e.classList.remove("active");
    }
  });
  e.target.classList.add("active");
}

tabs.addEventListener("mousedown", clickHandler);
