"use strict";

class List {
  constructor(source, filterSrc, target = "root") {
    this.source = source;
    this.filterSrc = filterSrc;
    this.target = document.getElementById(target);
  }

  filter() {
    let newArray = [];
    for (const object of this.source) {
      let counter = 0;
      for (let count = 0; count < this.filterSrc.length; count++) {
        try {
          if (!object.hasOwnProperty(this.filterSrc[count])) {
            throw new Error(
              `object ${object} don't contain ${this.filterSrc[count]}`
            );
          }
        } catch (e) {
          console.log(e.message);
          counter++;
        }
      }
      if (counter === 0) {
        newArray.unshift(object);
      }
    }
    this.dest = newArray;
  }

  render() {
    let ul = document.createElement("ul");
    for (const object of this.dest) {
      let li = document.createElement("li");
      li.innerHTML = `<h2>${object.name}</h2>
                      <p>${object.author}</p>
                      <p>${object.price}</p>`;
      ul.prepend(li);
    }
    this.target.append(ul);
  }
}

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const filter = ["author", "name", "price"];
const filter1 = ["name", "price"];

const list = new List(books, filter, "root");
const list1 = new List(books, filter1, "uhh");

list.filter();
list.render();

list1.filter();
list1.render();
