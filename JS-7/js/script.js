"use strict";

// prettier-ignore
let array = ["Kharkiv", "Kiev", ["Borispol", "Irpin", ["New-York", "Berlin"]], "Odessa", "Lviv", "Dnieper"];

function createList(array, parent = document.body) {
  let resultUl = `<ul>${createUl(array).join("")}</ul>`;
  parent.innerHTML = resultUl;
}

function createUl(array) {
  return array.map((element) => {
    if (Array.isArray(element)) {
      return `<li><ul>${createUl(element).join("")}</ul></li>`;
    } else {
      return `<li>${element}</li>`;
    }
  });
}

function timer(delay) {
  let timerDiv = document.createElement("div");
  document.body.prepend(timerDiv);
  function tick() {
    timerDiv.textContent = delay;
    delay--;
    if (delay < 0) {
      document.body.innerHTML = "";
    } else {
      setTimeout(tick, 1000);
    }
  }
  setTimeout(tick, 1000);
}

createList(array);
timer(3);
