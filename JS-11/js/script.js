"use strict";

const buttons = document.querySelectorAll(".btn");

function keyHandler(e) {
  buttons.forEach((element) => {
    if (element.textContent.toUpperCase() === e.key.toUpperCase()) {
      element.style.backgroundColor = "blue";
    } else {
      element.style.backgroundColor = "black";
    }
  });
}

document.addEventListener("keypress", keyHandler);
