"use strict";

function createNewUser() {
  let user = {
    getLogin() {
      return (this._firstName.slice(0, 1) + this._lastName).toLowerCase();
    },
    setConfigurableProperty(property, value) {
      Object.defineProperty(this, property, {
        value: value,
        configurable: true,
      });
    },
    writePrivateProperty(property, value) {
      Object.defineProperty(this, property, {
        writable: true,
        value: value,
      });
      Object.defineProperty(this, property, {
        writable: false,
      });
    },
    setFirstName(value) {
      this.writePrivateProperty("_firstName", value);
    },
    setLastName(value) {
      this.writePrivateProperty("_lastName", value);
    },
    getAge() {
      let now = Date.now();
      let age =
        now - new Date(this.birthday.split(".").reverse().toString()).getTime();
      return Math.trunc(age / 1000 / 31536000);
    },
    getPassword() {
      return (
        this._firstName.slice(0, 1).toUpperCase() +
        this._lastName.toLowerCase() +
        this.birthday.split(".")[2]
      );
    },
  };

  user.setConfigurableProperty(
    "_firstName",
    prompt("Enter user firstname").trim()
  );
  user.setConfigurableProperty(
    "_lastName",
    prompt("Enter user lastname").trim()
  );
  user.birthday = prompt(
    "Enter birthday in format dd.mm.yyyy",
    "dd.mm.yyyy"
  ).trim();
  return user;
}

let user1 = createNewUser();
let user2 = createNewUser();

console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());
console.log("---------");
console.log(user2.getLogin());
console.log(user2.getAge());
console.log(user2.getPassword());
