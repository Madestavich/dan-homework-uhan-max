"use strict";

let userName = prompt("Enter your name, please", "");
let userAge = prompt("Enter your age, please", "");

while (userName.trim() === "") {
  userName = prompt("Enter your name, please", userName);
}

while (!userAge || isNaN(+userAge) || userAge.trim() === "") {
  userAge = prompt("Enter your age, please", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge <= 22) {
  let confirmation = confirm("Are you sure you want to continue?");
  if (confirmation === true) {
    alert("Welcome, " + userName + "!");
  } else {
    alert("You are not allowed to visit this website!");
  }
} else {
  alert("Welcome, " + userName + "!");
}
