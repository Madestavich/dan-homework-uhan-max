"use strict";

const images = document.querySelectorAll(".image-to-show");
const buttons = document.querySelectorAll("button");
let running = true;

let timer = setInterval(changeImage, 3000);

buttons[0].addEventListener("click", stopHandler);

function changeImage() {
  let imageIndex;
  for (const [index, element] of images.entries()) {
    if (element.classList.contains("show")) {
      element.classList.toggle("show");
      imageIndex = index;
    }
  }
  if (imageIndex + 1 < images.length) {
    images[imageIndex + 1].classList.toggle("show");
  } else {
    images[0].classList.toggle("show");
  }
}

function startHandler() {
  timer = setInterval(changeImage, 3000);
  buttons[1].removeEventListener("click", startHandler);
  buttons[0].addEventListener("click", stopHandler);
}

function stopHandler() {
  clearInterval(timer);
  buttons[0].removeEventListener("click", stopHandler);
  buttons[1].addEventListener("click", startHandler);
}
