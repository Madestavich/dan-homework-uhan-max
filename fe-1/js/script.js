"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }

  set age(age) {
    this._name = age;
  }

  set salary(salary) {
    this._name = salary;
  }
}

class Programmer extends Employee {
  constructor(lang, name, age, salary) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const petya = new Programmer(["turbo pascal"], "Petya", 85, 300);
const vasya = new Programmer(["visual basic"], "Vasya", 73, 420);
const siroga = new Programmer(
  ["C#", "scala", "phyton"],
  "Siroga Computorshik",
  15,
  1000000
);

console.log(petya, vasya, siroga);
console.log(petya.salary, vasya.salary, siroga.salary);
