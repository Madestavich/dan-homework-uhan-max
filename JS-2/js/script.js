"use strict";

//* Перше завдання

let userNumber;

while (!Number.isInteger(+userNumber)) {
  userNumber = prompt("Enter integer number");
}

if (!userNumber || userNumber.trim() === "") {
  console.log("Sorry, no numbers");
} else if (userNumber >= 0) {
  for (let i = 0; i <= userNumber; i += 5) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
} else {
  for (let i = 0; i > userNumber; i -= 5) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}

//* Друге завдання - прості числа

// let firstNum;
// let secondNum;

// while (!Number.isInteger(+firstNum)) {
//   firstNum = prompt("enter first integer number");
// }

// while (!Number.isInteger(+secondNum)) {
//   secondNum = prompt("enter second integer number");
// }

// let flag = true;

// for (let checkNum = +firstNum; checkNum <= +secondNum; checkNum++) {
//   for (let i = 2; i < checkNum; i++) {
//     if (checkNum % i === 0) {
//       flag = false;
//       break;
//     }
//   }
//   if (flag) {
//     console.log(checkNum);
//   } else {
//     flag = true;
//   }
// }
