"use strict";

let root = document.getElementById("root");

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((data) => {
    getCharacters(data, insertCharacterData);
    render(data);
  })
  .then();

function render(films) {
  let ul = document.createElement("ul");
  for (let film of films) {
    let li = document.createElement("li");
    li.innerHTML = `<h2>Episode ${film.episodeId}</h2>
                      <p>${film.name}</p>
                      <ul id="episode-${film.episodeId}"></ul>
                      <p>${film.openingCrawl}</p>`;
    ul.append(li);
  }
  root.append(ul);
}

function getCharacters(data, action) {
  data.forEach((film) => {
    film.characters.forEach((characterData) => {
      fetch(characterData).then((response) =>
        response.json().then((character) => action(character, film))
      );
    });
  });
}

function insertCharacterData(character, film) {
  let li = document.createElement("li");
  li.textContent = character.name;
  document.getElementById(`episode-${film.episodeId}`).append(li);
}
