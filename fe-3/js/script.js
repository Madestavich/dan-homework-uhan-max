"use strict";

//! task 1 ----------------------------------------------------------------

const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

//* answer ----------------------------------------------------------------

const clients3 = [...new Set([...clients1, ...clients2])];

//! task 2 ----------------------------------------------------------------

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

//* answer ----------------------------------------------------------------

let charactersShortInfo = [];

characters.forEach((element) => {
  let { name, lastName, age } = element;
  charactersShortInfo.push({ name, lastName, age });
});

//! task 3 ----------------------------------------------------------------

const user1 = {
  name: "John",
  years: 30,
};

//* answer ----------------------------------------------------------------

let { name, years: age, isAdmin = false } = user1;

//! task 4 ----------------------------------------------------------------

const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

//* answer ----------------------------------------------------------------

let satoshi = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

//! task 5 ----------------------------------------------------------------

const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};

//* answer ----------------------------------------------------------------

let newBooks = [...books, bookToAdd];

//! task 6 ----------------------------------------------------------------

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

//* answer ----------------------------------------------------------------

let newEmployee = { ...employee, age: "", salary: "" };

//! task 7 ----------------------------------------------------------------

const array = ["value", () => "showValue"];

//* answer ----------------------------------------------------------------

let [value, showValue] = array;

//? logs ------------------------------------------------------------------

console.log("Task 1", clients3);
console.log("Task 2", charactersShortInfo);
console.log("Task 3", name, age, isAdmin);
console.log("Task 4", satoshi);
console.log("Task 5", "oldObj:", books, "newObj:", newBooks);
console.log("Task 6", "oldEmployee:", books, "newEmployee:", newBooks);
console.log("Task 7", value, showValue);
