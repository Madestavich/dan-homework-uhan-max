"use strict";

let themeItems = document.querySelectorAll(".theme");
let themeBtn = document.getElementById("theme");

function setTheme(theme, option) {
  if (option) {
    themeItems.forEach((element) => {
      element.classList.add(theme);
    });
    localStorage.setItem("theme", JSON.stringify(theme));
  } else {
    themeItems.forEach((element) => {
      element.classList.remove(theme);
    });
    localStorage.setItem("theme", JSON.stringify("classic"));
  }
}

themeBtn.addEventListener("click", () => {
  if (JSON.parse(localStorage.getItem("theme")) === "night-theme") {
    setTheme("night-theme");
  } else {
    setTheme("night-theme", true);
  }
});

function checkTheme() {
  if (JSON.parse(localStorage.getItem("theme")) === "night-theme") {
    setTheme("night-theme", true);
  }
}

document.addEventListener("DOMContentLoaded", checkTheme);
