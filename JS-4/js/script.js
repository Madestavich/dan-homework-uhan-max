"use strict";

function createNewUser() {
  function writePrivateProperty(object, property, value) {
    Object.defineProperty(object, property, {
      writable: true,
      value: value,
    });
    Object.defineProperty(object, property, {
      writable: false,
    });
  }

  function makePropUnSet(object, properties) {
    properties.forEach((property) =>
      Object.defineProperty(object, property, {
        writable: false,
      })
    );
    return object;
  }

  let user = {
    _firstName: prompt("Enter user firstname").trim(),
    _lastName: prompt("Enter user lastname").trim(),
    birthday: prompt(
      "Enter birthday in format dd.mm.yyyy",
      "dd.mm.yyyy"
    ).trim(),
    getLogin() {
      return (this._firstName.slice(0, 1) + this._lastName).toLowerCase();
    },
    setFirstName(value) {
      writePrivateProperty(this, "_firstName", value);
    },
    setLastName(value) {
      writePrivateProperty(this, "_lastName", value);
    },
    getAge() {
      let now = Date.now();
      let age =
        now - new Date(this.birthday.split(".").reverse().toString()).getTime();
      return Math.trunc(age / 1000 / 31536000);
    },
    getPassword() {
      return (
        this._firstName.slice(0, 1).toUpperCase() +
        this._lastName.toLowerCase() +
        this.birthday.split(".")[2]
      );
    },
  };
  return makePropUnSet(user, ["_firstName", "_lastName"]);
}

let user1 = createNewUser();

console.log(user1.getLogin());
