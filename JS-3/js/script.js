"use strict";

let firstNum;
let secondNum;
let operation;

while (!firstNum || isNaN(+firstNum) || firstNum.trim() === "") {
  firstNum = prompt("Enter first number", firstNum);
}

while (!secondNum || isNaN(+secondNum) || secondNum.trim() === "") {
  secondNum = prompt("Enter second number", secondNum);
}

while (
  operation != "+" &&
  operation != "-" &&
  operation != "*" &&
  operation != "/"
) {
  operation = prompt("Enter operation", operation);
}

function calculate(num1, num2, oper) {
  switch (oper) {
    case "+":
      return +num1 + +num2;
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      return num1 / num2;
  }
}

let result = calculate(firstNum, secondNum, operation);
console.log(result);
