"use strict";

let priceInput = document.getElementById("price");
let priceLabel = document.getElementById("price-label");
let div = document.getElementsByTagName("div");

let currentPrice = document.createElement("span");
let closeBtn = document.createElement("button");

priceInput.addEventListener("focus", inputFocusHandler);
priceInput.addEventListener("focusout", inputFocusOutHandler);
closeBtn.addEventListener("click", closeBtnHandler);

function inputFocusHandler() {
  this.style.outline = "green auto 1px";
  div[1].textContent = "";
}

function inputFocusOutHandler() {
  this.style.outline = "none";
  if (this.value < 0) {
    this.style.outline = "red auto 1px";
    this.style.color = "black";
    div[1].textContent = "Please enter correct price";
    div[0].removeChild(currentPrice);
    div[0].removeChild(closeBtn);
  } else if (this.value !== "") {
    currentPrice.textContent = `Current price: ${this.value}`;
    closeBtn.textContent = "x";
    div[0].append(currentPrice);
    div[0].append(closeBtn);
    this.style.color = "green";
  }
}

function closeBtnHandler() {
  div[0].removeChild(currentPrice);
  div[0].removeChild(closeBtn);
  priceInput.value = "";
  priceInput.style.outline = "none";
  priceInput.style.color = "black";
}
