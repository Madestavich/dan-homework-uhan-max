"use strict";

$(function () {
  let $navListLinks = $(".nav-list-link");

  $navListLinks.on("click", function (e) {
    e.preventDefault();
    let $offset = $($(this).attr("href")).offset();
    $("html").animate({ scrollTop: $offset.top }, 1000);
  });

  let $upBtn = $(".up-btn");

  $(window).scroll(function () {
    if (window.scrollY < window.screen.height) {
      $upBtn.fadeOut(100);
    } else {
      $upBtn.fadeIn(100);
    }
  });

  $upBtn.on("click", function () {
    $("html").animate({ scrollTop: 0 }, 500);
  });

  let $hideBtn = $("#hide-section");

  $hideBtn.on("click", function () {
    $("#news").slideToggle("slow", function () {
      if ($hideBtn.html() === "Hide") {
        $hideBtn.html("Show");
      } else {
        $hideBtn.html("Hide");
      }
    });
  });
});
