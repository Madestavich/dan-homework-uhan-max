"use strict";

function filterBy(array, type) {
  return array.filter((element) => typeof element !== type);
}

const array = ["hello", 666, NaN, {}, null, true];

console.log(filterBy(array, "number"));
console.log(filterBy(array, "string"));
console.log(filterBy(array, "object"));
console.log(filterBy(array, "boolean"));
