"use strict";

let passIcon = document.querySelectorAll(".icon-password");
let passInput = document.querySelectorAll(".input-wrapper > input");
let btn = document.getElementsByClassName("btn");

for (let i = 0; i < passIcon.length; i++) {
  passIcon[i].addEventListener("click", () => {
    passIcon[i].classList.toggle("fa-eye");
    passIcon[i].classList.toggle("fa-eye-slash");
    if (passIcon[i].classList.contains("fa-eye-slash")) {
      passInput[i].type = "text";
    } else {
      passInput[i].type = "password";
    }
  });
}

btn[0].addEventListener("click", () => {
  if (passInput[0].value === passInput[1].value) {
    console.log("ddd");
    alert("You are welcome");
  } else {
    let p = document.createElement("p");
    p.textContent = "Нужно ввести одинаковые значения";
    p.style.color = "red";
    passInput[1].after(p);
    setTimeout(() => {
      document.querySelector("p").remove();
    }, 500);
  }
});
